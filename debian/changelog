libchi-perl (0.61-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libjson-maybexs-perl,
      libmoo-perl.
    + libchi-perl: Drop versioned constraint on libjson-maybexs-perl,
      libmoo-perl in Depends.
  * Remove constraints unnecessary since buster
    * libchi-perl: Drop versioned constraint on libcache-fastmmap-perl
      in Depends.

  [ gregor herrmann ]
  * Import upstream version 0.61.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.6.0.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.
  * Drop unneeded alternatives from (build) dependencies.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Fri, 15 Oct 2021 18:00:27 +0200

libchi-perl (0.60-4) unstable; urgency=medium

  [ gregor herrmann ]
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.
  * Remove Chris Butler from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Add patch from CPAN RT for compatibility with Cache::FastMmap >= 1.45.
    (Closes: #870235)
  * Make dependency on libcache-fastmmap-perl versioned.
  * Remove unneeded version from libtry-tiny-perl (build) dependency.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.0.0.

 -- gregor herrmann <gregoa@debian.org>  Mon, 31 Jul 2017 14:41:40 -0400

libchi-perl (0.60-3) unstable; urgency=medium

  * Team upload.
  * Skip two autopkgtest failures due to ci.debian.net limitations
    (Closes: #800381)

 -- Niko Tyni <ntyni@debian.org>  Mon, 28 Sep 2015 20:51:36 +0300

libchi-perl (0.60-2) unstable; urgency=medium

  * Add (build) dependency on libtimedate-perl.
    This is an unreported FTBFS bug detected by ci.debian.net.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Wed, 12 Aug 2015 20:01:10 +0200

libchi-perl (0.60-1) unstable; urgency=medium

  * Import upstream version 0.60.
  * Update (build) dependencies.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 27 Jun 2015 23:29:27 +0200

libchi-perl (0.59-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Christopher Hoskin ]
  * New upstream release, added autopkgtest

  [ gregor herrmann ]
  * Explicitly (build) depend on libclass-load-perl.
  * Add debian/upstream/metadata

 -- Christopher Hoskin <christopher.hoskin@gmail.com>  Sun, 24 May 2015 09:50:39 +0100

libchi-perl (0.58-1) unstable; urgency=low

  [ Nathan Handler ]
  * New upstream release

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Xavier Guimard ]
  * New upstream release
  * Add myself to uploaders and copyright

  [ gregor herrmann ]
  * New upstream release.
  * Update years of upstream and packaging copyright.
  * Update (build) dependencies.
  * Declare compliance with Debian Policy 3.9.4.

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 Aug 2013 09:40:03 +0200

libchi-perl (0.54-1) unstable; urgency=low

  * Team upload
  * New upstream release
  * Drop fix_bad_whatis.patch and fix_pod.patch (merged upstream)
  * Add libstring-rewriteprefix-perl to (build) depends
  * Add NEWS file with incompatible changes

 -- Alessandro Ghedini <ghedo@debian.org>  Thu, 31 May 2012 21:32:18 +0200

libchi-perl (0.52-1) unstable; urgency=low

  * Team upload

  [ Alessandro Ghedini ]
  * New upstream release
  * Update debian/copyright format as in Debian Policy 3.9.3
  * Bump Standards-Version to 3.9.3
  * Update and refresh fix_pod.patch
  * (Re-)forward patches upstream

  [ Salvatore Bonaccorso ]
  * Simplify (Build-)Depends(-Indep) dependencies.
    Make versioned dependecies already satisfied in stable now unversioned.
    Lenny is moving to the archived releases.

 -- Alessandro Ghedini <al3xbio@gmail.com>  Fri, 09 Mar 2012 16:08:09 +0100

libchi-perl (0.50-1) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Imported Upstream version 0.50
  * Refresh fix_pod.patch patch for offsets

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 03 Dec 2011 17:26:18 +0100

libchi-perl (0.49-1) unstable; urgency=low

  * Team upload.
  * New upstream release

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 23 Jun 2011 23:26:50 +0200

libchi-perl (0.48-1) unstable; urgency=low

  * Team upload.
  * New upstream release

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 15 Jun 2011 18:37:22 +0200

libchi-perl (0.47-1) unstable; urgency=low

  * Team upload.
  * New upstream release
  * debian/patches:
    - Refresh patch fix_pod.patch (offset)
    - Refresh patch fix_bad_whatis.patch (offset)

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 30 Apr 2011 22:01:17 +0200

libchi-perl (0.46-1) unstable; urgency=low

  * New upstream release.
  * Refresh patch fix_pod.patch (offset).
  * Set Standards-Version to 3.9.2 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 Apr 2011 17:32:35 +0200

libchi-perl (0.44-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release 0.42
  * Add myself to Uploaders and Copyright
  * Bump to debhelper compat 8

  [ gregor herrmann ]
  * New upstream release 0.44.
  * Refresh patch fix_pod.patch (offset).
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Fri, 18 Mar 2011 23:41:16 +0100

libchi-perl (0.41-1) unstable; urgency=low

  [ Angel Abad ]
  * New upstream release
  * Add myself to Uploaders
  * debian/patches/fix_pod.patch: Refresh patch
  * debian/patches/fix_bad_whatis.patch:
    - Remove pod entries to fix bad whatis problem

  [ gregor herrmann ]
  * Remove (build) dependencies: liblog-any-adapter-dispatch-perl,
    libtest-log-dispatch-perl.

 -- Angel Abad <angelabad@gmail.com>  Thu, 03 Mar 2011 10:56:50 +0100

libchi-perl (0.39-1) unstable; urgency=low

  * New upstream release
  * Refreshed copyright and control files
    - updated years of upstream copyright
    - added myself to Uploaders / copyright
    - added new dependency on libtry-tiny-perl (>= 0.05)
    - dropped versioned dependency on debhelper to 7 - no longer using
      Module::Install
  * Refreshed fix_pod.patch

 -- Chris Butler <chrisb@debian.org>  Sat, 19 Feb 2011 18:50:05 +0000

libchi-perl (0.36-1) unstable; urgency=low

  * Initial Release (Closes: #593640).

 -- Ernesto Hernández-Novich (USB) <emhn@usb.ve>  Thu, 19 Aug 2010 15:37:58 -0430
